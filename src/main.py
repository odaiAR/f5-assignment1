import urllib.request

def checkifstringisinurl(s,url):
    f = urllib.request.urlopen(url)
    myfile = f.read()
    str = bytes(s, 'utf-8')
    if str in myfile:
        return True
    return False

