import pytest
import urllib.request

def checkifstringisinurl(s,url):
    f = urllib.request.urlopen(url)
    myfile = f.read()
    str = bytes(s, 'utf-8')
    if str in myfile:
        return True
    return False

def test_main():
    assert checkifstringisinurl("Right-click in the box below to see one called \'the-internet\'","https://the-internet.herokuapp.com/context_menu") is True
    
